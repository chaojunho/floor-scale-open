﻿using System.Threading.Tasks;
using Shouldly;
using Xunit;

namespace FloorScale.Pages
{
    public class Index_Tests : FloorScaleWebTestBase
    {
        [Fact]
        public async Task Welcome_Page()
        {
            var response = await GetResponseAsStringAsync("/");
            response.ShouldNotBeNull();
        }
    }
}
