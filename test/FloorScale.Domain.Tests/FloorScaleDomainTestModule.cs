﻿using FloorScale.EntityFrameworkCore;
using Volo.Abp.Modularity;

namespace FloorScale
{
    [DependsOn(
        typeof(FloorScaleEntityFrameworkCoreTestModule)
        )]
    public class FloorScaleDomainTestModule : AbpModule
    {

    }
}