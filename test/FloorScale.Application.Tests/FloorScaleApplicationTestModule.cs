﻿using Volo.Abp.Modularity;

namespace FloorScale
{
    [DependsOn(
        typeof(FloorScaleApplicationModule),
        typeof(FloorScaleDomainTestModule)
        )]
    public class FloorScaleApplicationTestModule : AbpModule
    {

    }
}