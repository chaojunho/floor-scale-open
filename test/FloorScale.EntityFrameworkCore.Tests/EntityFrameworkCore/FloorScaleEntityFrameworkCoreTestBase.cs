﻿using Volo.Abp;

namespace FloorScale.EntityFrameworkCore
{
    public abstract class FloorScaleEntityFrameworkCoreTestBase : FloorScaleTestBase<FloorScaleEntityFrameworkCoreTestModule> 
    {

    }
}
