﻿using System.Threading.Tasks;

namespace FloorScale.Data
{
    public interface IFloorScaleDbSchemaMigrator
    {
        Task MigrateAsync();
    }
}
