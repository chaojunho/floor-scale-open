﻿using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;

namespace FloorScale.Data
{
    /* This is used if database provider does't define
     * IFloorScaleDbSchemaMigrator implementation.
     */
    public class NullFloorScaleDbSchemaMigrator : IFloorScaleDbSchemaMigrator, ITransientDependency
    {
        public Task MigrateAsync()
        {
            return Task.CompletedTask;
        }
    }
}