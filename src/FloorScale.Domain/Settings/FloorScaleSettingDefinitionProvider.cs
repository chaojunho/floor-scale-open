﻿using Volo.Abp.Settings;

namespace FloorScale.Settings
{
    public class FloorScaleSettingDefinitionProvider : SettingDefinitionProvider
    {
        public override void Define(ISettingDefinitionContext context)
        {
            //Define your own settings here. Example:
            //context.Add(new SettingDefinition(FloorScaleSettings.MySetting1));
        }
    }
}
