﻿using Volo.Abp;

namespace FloorScale
{
    public class AlreadyExistsNameException : BusinessException
    {
        /// <summary>
        /// 名称存在
        /// </summary>
        /// <param name="input"></param>
        public AlreadyExistsNameException(string input)
            : base(FloorScaleDomainErrorCodes.NameAlreadyExists)
        {
            WithData("name", input);
        }
    }
}
