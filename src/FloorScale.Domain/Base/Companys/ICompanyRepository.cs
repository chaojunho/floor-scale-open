using System;
using Volo.Abp.Domain.Repositories;

namespace FloorScale.Base.Companys
{
    public interface ICompanyRepository : IRepository<Company, Guid>
    {
    }
}