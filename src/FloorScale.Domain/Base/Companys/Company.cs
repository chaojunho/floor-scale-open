using System;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.MultiTenancy;

namespace FloorScale.Base.Companys
{
    /// <summary>
    /// 企业信息
    /// </summary>
    public class Company : FullAuditedAggregateRoot<Guid>, IMultiTenant
    {
        public Guid? TenantId { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 机构编码
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 营业执照
        /// </summary>
        public string BusinessLicense { get; set; }
        /// <summary>
        /// 联系人
        /// </summary>
        public string Contact { get; set; }
        /// <summary>
        /// 电话
        /// </summary>
        public string Telephone { get; set; }
        /// <summary>
        /// 邮箱
        /// </summary>
        public string Mail { get; set; }
        /// <summary>
        /// 地址
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string Describe { get; set; }

        protected Company()
        {
        }

        public Company(
            Guid id,
            Guid? tenantId,
            string name,
            string code,
            string businessLicense,
            string contact,
            string telephone,
            string mail,
            string address,
            string describe
        ) : base(id)
        {
            TenantId = tenantId;
            Name = name;
            Code = code;
            BusinessLicense = businessLicense;
            Contact = contact;
            Telephone = telephone;
            Mail = mail;
            Address = address;
            Describe = describe;
        }
    }
}
