using System;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.MultiTenancy;

namespace FloorScale.Base.MaterialTypes
{
    /// <summary>
    /// 物料类型
    /// </summary>
    public class MaterialType : FullAuditedAggregateRoot<Guid>, IMultiTenant
    {
        public Guid? TenantId { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 编码
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public bool Status { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Orders { get; set; }

        protected MaterialType()
        {
        }

        public MaterialType(
            Guid id,
            Guid? tenantId,
            string name,
            string code,
            bool status,
            string remark,
            int orders
        ) : base(id)
        {
            TenantId = tenantId;
            Name = name;
            Code = code;
            Status = status;
            Remark = remark;
            Orders = orders;
        }
    }
}
