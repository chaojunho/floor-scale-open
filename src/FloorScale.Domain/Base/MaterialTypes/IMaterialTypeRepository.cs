using System;
using Volo.Abp.Domain.Repositories;

namespace FloorScale.Base.MaterialTypes
{
    public interface IMaterialTypeRepository : IRepository<MaterialType, Guid>
    {
    }
}