using System;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.MultiTenancy;

namespace FloorScale.Base.CarTypes
{
    /// <summary>
    /// 车辆类型
    /// </summary>
    public class CarType : FullAuditedAggregateRoot<Guid>, IMultiTenant
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 编码
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public bool Status { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Orders { get; set; }

        public Guid? TenantId { get; set; }

        protected CarType()
        {
        }

        public CarType(
            Guid id,
            string name,
            string code,
            bool status,
            string remark,
            int orders,
            Guid? tenantId
        ) : base(id)
        {
            Name = name;
            Code = code;
            Status = status;
            Remark = remark;
            Orders = orders;
            TenantId = tenantId;
        }
    }
}
