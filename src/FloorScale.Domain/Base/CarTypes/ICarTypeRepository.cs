using System;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace FloorScale.Base.CarTypes
{
    /// <summary>
    /// ��������
    /// </summary>
    public interface ICarTypeRepository :
        IRepository<CarType, Guid>
    {
        Task<CarType> FindByNameAsync(string input);
    }
}