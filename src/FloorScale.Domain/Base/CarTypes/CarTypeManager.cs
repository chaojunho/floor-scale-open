﻿using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Volo.Abp;
using Volo.Abp.Domain.Services;

namespace FloorScale.Base.CarTypes
{
    public class CarTypeManager : DomainService
    {
        private readonly ICarTypeRepository _repository;

        public CarTypeManager(
            ICarTypeRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// 检查创建数据
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<CarType> CreateAsync(CarType input)
        {
            Check.NotNullOrWhiteSpace(input.Name, nameof(input.Name));

            var existingCarType = await _repository.FindByNameAsync(input.Name);
            if (existingCarType != null)
            {
                throw new AlreadyExistsNameException(input.Name);
            }

            return input;
        }

        /// <summary>
        /// 改变名称
        /// 特殊情况，不允许外部改变名称
        /// </summary>
        /// <param name="input"></param>
        /// <param name="newName"></param>
        /// <returns></returns>
        public async Task ChangeNameAsync(
            [NotNull] CarType input,
            [NotNull] string newName)
        {
            Check.NotNull(input, nameof(input));
            Check.NotNullOrWhiteSpace(newName, nameof(newName));

            var existing = await _repository.FindByNameAsync(newName);
            if (existing != null && existing.Id != input.Id)
            {
                throw new AlreadyExistsNameException(newName);
            }

            input.Name = newName;
        }
    }
}
