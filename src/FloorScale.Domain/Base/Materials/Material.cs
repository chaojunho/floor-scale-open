using System;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.MultiTenancy;

namespace FloorScale.Base.Materials
{
    /// <summary>
    /// 物料信息
    /// </summary>
    public class Material : FullAuditedAggregateRoot<Guid>, IMultiTenant
    {
        public Guid? TenantId { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 编码
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 类型编号
        /// </summary>
        public Guid? TypeId { get; set; }
        /// <summary>
        /// 单价
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public bool Status { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Orders { get; set; }

        protected Material()
        {
        }

        public Material(
            Guid id,
            Guid? tenantId,
            string name,
            string code,
            Guid? typeId,
            decimal price,
            bool status,
            string remark,
            int orders
        ) : base(id)
        {
            TenantId = tenantId;
            Name = name;
            Code = code;
            TypeId = typeId;
            Price = price;
            Status = status;
            Remark = remark;
            Orders = orders;
        }
    }
}
