using System;
using Volo.Abp.Domain.Repositories;

namespace FloorScale.Base.Materials
{
    public interface IMaterialRepository : IRepository<Material, Guid>
    {
    }
}