using System;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.MultiTenancy;

namespace FloorScale.Base.Logs
{
    /// <summary>
    /// 操作日志
    /// </summary>
    public class Log : CreationAuditedAggregateRoot<Guid>, IMultiTenant
    {
        public Guid? TenantId { get; set; }

        /// <summary>
        /// 动作
        /// </summary>
        public string Action { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 客户端编号
        /// </summary>
        public string FrmClientNO { get; set; }

        protected Log()
        {
        }

        public Log(
            Guid id,
            Guid? tenantId,
            string action,
            string remark,
            string frmClientNO
        ) : base(id)
        {
            TenantId = tenantId;
            Action = action;
            Remark = remark;
            FrmClientNO = frmClientNO;
        }
    }
}
