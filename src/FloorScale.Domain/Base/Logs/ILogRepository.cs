using System;
using Volo.Abp.Domain.Repositories;

namespace FloorScale.Base.Logs
{
    public interface ILogRepository : IRepository<Log, Guid>
    {
    }
}