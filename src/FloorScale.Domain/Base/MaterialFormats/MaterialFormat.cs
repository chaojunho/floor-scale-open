using System;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.MultiTenancy;

namespace FloorScale.Base.MaterialFormats
{
    /// <summary>
    /// 物料规格
    /// </summary>
    public class MaterialFormat : FullAuditedAggregateRoot<Guid>, IMultiTenant
    {
        public Guid? TenantId { get; set; }

        /// <summary>
        /// 物料编号
        /// </summary>
        public Guid MaterialId { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 单价
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public bool Status { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Orders { get; set; }

        protected MaterialFormat()
        {
        }

        public MaterialFormat(
            Guid id,
            Guid? tenantId,
            Guid materialId,
            string name,
            decimal price,
            bool status,
            string remark,
            int orders
        ) : base(id)
        {
            TenantId = tenantId;
            MaterialId = materialId;
            Name = name;
            Price = price;
            Status = status;
            Remark = remark;
            Orders = orders;
        }
    }
}
