using System;
using Volo.Abp.Domain.Repositories;

namespace FloorScale.Base.MaterialFormats
{
    public interface IMaterialFormatRepository : IRepository<MaterialFormat, Guid>
    {
    }
}