using JetBrains.Annotations;
using System;
using Volo.Abp;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.MultiTenancy;

namespace FloorScale.Base.Cars
{
    /// <summary>
    /// 车辆信息
    /// </summary>
    public class Car : FullAuditedAggregateRoot<Guid>, IMultiTenant
    {
        public Guid? TenantId { get; set; }

        /// <summary>
        /// 车牌号码
        /// </summary>
        public string LicensePlate { get; set; }
        /// <summary>
        /// 车主姓名
        /// </summary>
        public string CarOwnerName { get; set; }
        /// <summary>
        /// 车主身份证
        /// </summary>
        public string CarOwnerIDCard { get; set; }
        /// <summary>
        /// 车主电话
        /// </summary>
        public string CarOwnerPhone { get; set; }
        /// <summary>
        /// 车辆类型编号
        /// </summary>
        public Guid? TypeId { get; set; }
        /// <summary>
        /// 送货单位编号
        /// </summary>
        public Guid? ShipperId { get; set; }
        /// <summary>
        /// 收货单位编号
        /// </summary>
        public Guid? ReceivingUnitId { get; set; }
        /// <summary>
        /// 物料编号
        /// </summary>
        public Guid? MaterialId { get; set; }
        /// <summary>
        /// 物料规格编号
        /// </summary>
        public Guid? MaterialFormatId { get; set; }
        /// <summary>
        /// 过磅类型编号
        /// </summary>
        public Guid? WeightTypeId { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public bool Status { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Orders { get; set; }

        private void SetName([NotNull] string name)
        {
            LicensePlate = Check.NotNullOrWhiteSpace(
                name,
                nameof(name),
                maxLength: FloorScaleDomainConsts.FieldLen20
            );
        }

        internal Car ChangeName([NotNull] string name)
        {
            SetName(name);
            return this;
        }

        protected Car()
        {
        }

        public Car(
            Guid id,
            Guid? tenantId,
            string licensePlate,
            string carOwnerName,
            string carOwnerIDCard,
            string carOwnerPhone,
            Guid? typeId,
            Guid? shipperId,
            Guid? receivingUnitId,
            Guid? materialId,
            Guid? materialFormatId,
            Guid? weightTypeId,
            bool status,
            string remark,
            int orders
        ) : base(id)
        {
            TenantId = tenantId;
            LicensePlate = licensePlate;
            CarOwnerName = carOwnerName;
            CarOwnerIDCard = carOwnerIDCard;
            CarOwnerPhone = carOwnerPhone;
            TypeId = typeId;
            ShipperId = shipperId;
            ReceivingUnitId = receivingUnitId;
            MaterialId = materialId;
            MaterialFormatId = materialFormatId;
            WeightTypeId = weightTypeId;
            Status = status;
            Remark = remark;
            Orders = orders;
        }
    }
}
