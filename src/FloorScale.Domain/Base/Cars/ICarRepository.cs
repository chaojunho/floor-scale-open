using System;
using Volo.Abp.Domain.Repositories;

namespace FloorScale.Base.Cars
{
    public interface ICarRepository : IRepository<Car, Guid>
    {
    }
}