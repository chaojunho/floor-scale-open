﻿using FloorScale.Localization;
using Volo.Abp.AspNetCore.Mvc;

namespace FloorScale.Controllers
{
    /* Inherit your controllers from this class.
     */
    public abstract class FloorScaleController : AbpController
    {
        protected FloorScaleController()
        {
            LocalizationResource = typeof(FloorScaleResource);
        }
    }
}