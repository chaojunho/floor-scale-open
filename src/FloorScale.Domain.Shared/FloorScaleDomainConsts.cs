﻿namespace FloorScale
{
    public static class FloorScaleDomainConsts
    {
        public const int FieldLen4000 = 4000;
        public const int FieldLen1000 = 1000;
        public const int FieldLen500 = 500;
        public const int FieldLen200 = 200;
        public const int FieldLen100 = 100;
        public const int FieldLen50 = 50;
        public const int FieldLen30 = 30;
        public const int FieldLen20 = 20;
    }
}
