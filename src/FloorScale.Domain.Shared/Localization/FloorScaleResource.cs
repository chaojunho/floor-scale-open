﻿using Volo.Abp.Localization;

namespace FloorScale.Localization
{
    [LocalizationResourceName("FloorScale")]
    public class FloorScaleResource
    {

    }
}