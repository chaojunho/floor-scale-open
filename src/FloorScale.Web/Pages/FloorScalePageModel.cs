﻿using FloorScale.Localization;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;

namespace FloorScale.Web.Pages
{
    /* Inherit your PageModel classes from this class.
     */
    public abstract class FloorScalePageModel : AbpPageModel
    {
        protected FloorScalePageModel()
        {
            LocalizationResourceType = typeof(FloorScaleResource);
        }
    }
}