﻿using Volo.Abp.Ui.Branding;
using Volo.Abp.DependencyInjection;

namespace FloorScale.Web
{
    [Dependency(ReplaceServices = true)]
    public class FloorScaleBrandingProvider : DefaultBrandingProvider
    {
        public override string AppName => "FloorScale";
    }
}
