﻿using System.Threading.Tasks;
using Volo.Abp.UI.Navigation;

namespace FloorScale.Web.Menus
{
    public class FloorScaleMenuContributor : IMenuContributor
    {
        public async Task ConfigureMenuAsync(MenuConfigurationContext context)
        {
            if (context.Menu.Name == StandardMenus.Main)
            {
                await ConfigureMainMenuAsync(context);
            }
        }

        private async Task ConfigureMainMenuAsync(MenuConfigurationContext context)
        {
            await Task.CompletedTask;

            //var administration = context.Menu.GetAdministration();
            //var l = context.GetLocalizer<FloorScaleResource>();

            //context.Menu.Items.Insert(
            //    0,
            //    new ApplicationMenuItem(
            //        FloorScaleMenus.Home,
            //        l["Menu:Home"],
            //        "~/",
            //        icon: "fas fa-home",
            //        order: 0
            //    )
            //);

            //if (MultiTenancyConsts.IsEnabled)
            //{
            //    administration.SetSubItemOrder(TenantManagementMenuNames.GroupName, 1);
            //}
            //else
            //{
            //    administration.TryRemoveMenuItem(TenantManagementMenuNames.GroupName);
            //}

            //administration.SetSubItemOrder(IdentityMenuNames.GroupName, 2);
            //administration.SetSubItemOrder(SettingManagementMenuNames.GroupName, 3);
        }
    }
}
