﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using FloorScale.Data;
using Volo.Abp.DependencyInjection;

namespace FloorScale.EntityFrameworkCore
{
    public class EntityFrameworkCoreFloorScaleDbSchemaMigrator
        : IFloorScaleDbSchemaMigrator, ITransientDependency
    {
        private readonly IServiceProvider _serviceProvider;

        public EntityFrameworkCoreFloorScaleDbSchemaMigrator(
            IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task MigrateAsync()
        {
            /* We intentionally resolving the FloorScaleDbContext
             * from IServiceProvider (instead of directly injecting it)
             * to properly get the connection string of the current tenant in the
             * current scope.
             */

            await _serviceProvider
                .GetRequiredService<FloorScaleDbContext>()
                .Database
                .MigrateAsync();
        }
    }
}
