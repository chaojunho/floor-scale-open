using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace FloorScale.Base.Logs
{
    public static class LogEfCoreQueryableExtensions
    {
        public static IQueryable<Log> IncludeDetails(this IQueryable<Log> queryable, bool include = true)
        {
            if (!include)
            {
                return queryable;
            }

            return queryable
                // .Include(x => x.xxx) // TODO: AbpHelper generated
                ;
        }
    }
}