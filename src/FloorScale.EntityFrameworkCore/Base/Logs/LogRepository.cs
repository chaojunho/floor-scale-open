using System;
using FloorScale.EntityFrameworkCore;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace FloorScale.Base.Logs
{
    public class LogRepository : EfCoreRepository<FloorScaleDbContext, Log, Guid>, ILogRepository
    {
        public LogRepository(IDbContextProvider<FloorScaleDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}