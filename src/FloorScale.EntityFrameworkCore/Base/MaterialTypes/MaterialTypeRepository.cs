using System;
using FloorScale.EntityFrameworkCore;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace FloorScale.Base.MaterialTypes
{
    public class MaterialTypeRepository : EfCoreRepository<FloorScaleDbContext, MaterialType, Guid>, IMaterialTypeRepository
    {
        public MaterialTypeRepository(IDbContextProvider<FloorScaleDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}