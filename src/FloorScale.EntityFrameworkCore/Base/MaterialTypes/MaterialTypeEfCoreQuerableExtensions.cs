using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace FloorScale.Base.MaterialTypes
{
    public static class MaterialTypeEfCoreQueryableExtensions
    {
        public static IQueryable<MaterialType> IncludeDetails(this IQueryable<MaterialType> queryable, bool include = true)
        {
            if (!include)
            {
                return queryable;
            }

            return queryable
                // .Include(x => x.xxx) // TODO: AbpHelper generated
                ;
        }
    }
}