using System;
using FloorScale.EntityFrameworkCore;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace FloorScale.Base.Companys
{
    public class CompanyRepository : EfCoreRepository<FloorScaleDbContext, Company, Guid>, ICompanyRepository
    {
        public CompanyRepository(IDbContextProvider<FloorScaleDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}