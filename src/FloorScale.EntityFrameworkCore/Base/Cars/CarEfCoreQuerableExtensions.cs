using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace FloorScale.Base.Cars
{
    public static class CarEfCoreQueryableExtensions
    {
        public static IQueryable<Car> IncludeDetails(this IQueryable<Car> queryable, bool include = true)
        {
            if (!include)
            {
                return queryable;
            }

            return queryable
                // .Include(x => x.xxx) // TODO: AbpHelper generated
                ;
        }
    }
}