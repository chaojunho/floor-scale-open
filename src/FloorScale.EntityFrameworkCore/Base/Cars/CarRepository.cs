using System;
using FloorScale.EntityFrameworkCore;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace FloorScale.Base.Cars
{
    public class CarRepository : EfCoreRepository<FloorScaleDbContext, Car, Guid>, ICarRepository
    {
        public CarRepository(IDbContextProvider<FloorScaleDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}