using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace FloorScale.Base.MaterialFormats
{
    public static class MaterialFormatEfCoreQueryableExtensions
    {
        public static IQueryable<MaterialFormat> IncludeDetails(this IQueryable<MaterialFormat> queryable, bool include = true)
        {
            if (!include)
            {
                return queryable;
            }

            return queryable
                // .Include(x => x.xxx) // TODO: AbpHelper generated
                ;
        }
    }
}