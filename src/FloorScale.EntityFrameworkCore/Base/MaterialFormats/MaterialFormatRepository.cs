using System;
using FloorScale.EntityFrameworkCore;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace FloorScale.Base.MaterialFormats
{
    public class MaterialFormatRepository : EfCoreRepository<FloorScaleDbContext, MaterialFormat, Guid>, IMaterialFormatRepository
    {
        public MaterialFormatRepository(IDbContextProvider<FloorScaleDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}