using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace FloorScale.Base.Materials
{
    public static class MaterialEfCoreQueryableExtensions
    {
        public static IQueryable<Material> IncludeDetails(this IQueryable<Material> queryable, bool include = true)
        {
            if (!include)
            {
                return queryable;
            }

            return queryable
                // .Include(x => x.xxx) // TODO: AbpHelper generated
                ;
        }
    }
}