using System;
using FloorScale.EntityFrameworkCore;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace FloorScale.Base.Materials
{
    public class MaterialRepository : EfCoreRepository<FloorScaleDbContext, Material, Guid>, IMaterialRepository
    {
        public MaterialRepository(IDbContextProvider<FloorScaleDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}