using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace FloorScale.Base.CarTypes
{
    public static class CarTypeEfCoreQueryableExtensions
    {
        public static IQueryable<CarType> IncludeDetails(this IQueryable<CarType> queryable, bool include = true)
        {
            if (!include)
            {
                return queryable;
            }

            return queryable
                // .Include(x => x.xxx) // TODO: AbpHelper generated
                ;
        }
    }
}