using System;
using System.Threading.Tasks;
using FloorScale.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace FloorScale.Base.CarTypes
{
    public class CarTypeRepository : EfCoreRepository<FloorScaleDbContext, CarType, Guid>, ICarTypeRepository
    {
        public CarTypeRepository(IDbContextProvider<FloorScaleDbContext> dbContextProvider) 
            : base(dbContextProvider)
        {
        }
        
        /// <summary>
        /// ����DbSet
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<CarType> FindByNameAsync(string input)
        {
            var dbSet = await GetDbSetAsync();
            return await dbSet
                .FirstOrDefaultAsync(p => p.Name == input);
        }
    }
}