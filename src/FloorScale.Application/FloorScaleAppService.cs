﻿using System;
using System.Collections.Generic;
using System.Text;
using FloorScale.Localization;
using Volo.Abp.Application.Services;

namespace FloorScale
{
    /* Inherit your application services from this class.
     */
    public abstract class FloorScaleAppService : ApplicationService
    {
        protected FloorScaleAppService()
        {
            LocalizationResource = typeof(FloorScaleResource);
        }
    }
}
