using FloorScale.Base.MaterialTypes.Dtos;
using FloorScale.Permissions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace FloorScale.Base.MaterialTypes
{
    public class MaterialTypeAppService : CrudAppService<MaterialType, MaterialTypeDto, Guid, PagedAndSortedResultRequestDto, CreateMaterialTypeDto, UpdateMaterialTypeDto>,
        IMaterialTypeAppService
    {
        protected override string GetPolicyName { get; set; } = FloorScalePermissions.MaterialType.Default;
        protected override string GetListPolicyName { get; set; } = FloorScalePermissions.MaterialType.Default;
        protected override string CreatePolicyName { get; set; } = FloorScalePermissions.MaterialType.Create;
        protected override string UpdatePolicyName { get; set; } = FloorScalePermissions.MaterialType.Update;
        protected override string DeletePolicyName { get; set; } = FloorScalePermissions.MaterialType.Delete;

        private readonly IMaterialTypeRepository _repository;
        
        public MaterialTypeAppService(IMaterialTypeRepository repository) : base(repository)
        {
            _repository = repository;
        }
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="id">编号</param>
        /// <returns></returns>
        public override async Task<MaterialTypeDto> GetAsync(Guid id)
        {
            var model = await _repository.GetAsync(id);
            return ObjectMapper.Map<MaterialType, MaterialTypeDto>(model);
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public override async Task<PagedResultDto<MaterialTypeDto>> GetListAsync(
            PagedAndSortedResultRequestDto input)
        {
            await CheckGetListPolicyAsync();

            if (input.Sorting.IsNullOrWhiteSpace())
            {
                input.Sorting = nameof(MaterialType.Orders) + " desc";
            }

            var query = _repository
                //.WhereIf(
                //    !input.Filter.IsNullOrWhiteSpace(),
                //    (p => p.Name.Contains(input.Filter))
                //)
                .OrderBy(input.Sorting)
                .Skip(input.SkipCount)
                .Take(input.MaxResultCount);

            var queryResult = await AsyncExecuter.ToListAsync(query);

            var infoDtos = queryResult.Select(x =>
            {
                var infoDto = ObjectMapper.Map<MaterialType, MaterialTypeDto>(x);
                return infoDto;
            }).ToList();

            var totalCount = await AsyncExecuter.CountAsync(
                _repository
                //.WhereIf(
                //    !input.Filter.IsNullOrWhiteSpace(),
                //    model => model.Name.Contains(input.Filter)
                //)
            );

            return new PagedResultDto<MaterialTypeDto>(
                totalCount,
                infoDtos);
        }

        /// <summary>
        /// 获取树状列表
        /// </summary>
        /// <returns></returns>
        public async Task<ListResultDto<TreeviewDto>> GetTreeviewAsync(string input)
        {
            var query = _repository
                .Where(p => p.Status == true)
                .WhereIf(
                    !input.IsNullOrWhiteSpace(),
                    model => model.Name.Contains(input) || model.Code.Contains(input)
                )
                .OrderByDescending(p => p.Orders);

            var queryResult = await AsyncExecuter.ToListAsync(query);

            return new ListResultDto<TreeviewDto>(
                ObjectMapper.Map<List<MaterialType>, List<TreeviewDto>>(
                    queryResult
                )
            );
        }

        /// <summary>
        /// 创建数据
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [Authorize(FloorScalePermissions.Material.Create)]
        public override async Task<MaterialTypeDto> CreateAsync(CreateMaterialTypeDto input)
        {
            input.TenantId = CurrentTenant.Id;
            var checkInput = await _repository.FindAsync(p => p.Name == input.Name);
            if (checkInput != null)
            {
                throw new AlreadyExistsNameException(input.Name);
            }

            var inputModel = ObjectMapper.Map<CreateMaterialTypeDto, MaterialType>(input);

            var model = await _repository.InsertAsync(inputModel);

            return ObjectMapper.Map<MaterialType, MaterialTypeDto>(model);
        }

        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="id"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        [Authorize(FloorScalePermissions.Material.Update)]
        public override async Task<MaterialTypeDto> UpdateAsync(Guid id, UpdateMaterialTypeDto input)
        {
            var model = await _repository.GetAsync(id);
            if (model.Name != input.Name)
            {
                var checkInput = await _repository.FindAsync(p => p.Name == input.Name);
                if (checkInput != null && checkInput.Id != id)
                {
                    throw new AlreadyExistsNameException(input.Name);
                }
                model.Name = input.Name;
            }
            model.Code = input.Code;
            model.Status = input.Status;
            model.Remark = input.Remark;
            model.Orders = input.Orders;

            await _repository.UpdateAsync(model);

            return ObjectMapper.Map<MaterialType, MaterialTypeDto>(model);
        }

        /// <summary>
        /// 删除实体
        /// </summary>
        /// <param name="id">编号</param>
        /// <returns></returns>
        [Authorize(FloorScalePermissions.Material.Delete)]
        public override async Task DeleteAsync(Guid id)
        {
            await _repository.DeleteAsync(id);
        }

        /// <summary>
        /// 获取最大排序
        /// </summary>
        /// <returns></returns>
        public async Task<int> GetOrdersAsync()
        {
            await CheckCreatePolicyAsync();

            return (_repository.Max(p => (int?)p.Orders) ?? 0) + 3;
        }
    }
}
