using FloorScale.Base.Materials.Dtos;
using FloorScale.Base.MaterialTypes;
using FloorScale.Permissions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace FloorScale.Base.Materials
{
    public class MaterialAppService : CrudAppService<Material, MaterialDto, Guid, PagedAndSortedResultRequestDto, CreateMaterialDto, UpdateMaterialDto>,
        IMaterialAppService
    {
        protected override string GetPolicyName { get; set; } = FloorScalePermissions.Material.Default;
        protected override string GetListPolicyName { get; set; } = FloorScalePermissions.Material.Default;
        protected override string CreatePolicyName { get; set; } = FloorScalePermissions.Material.Create;
        protected override string UpdatePolicyName { get; set; } = FloorScalePermissions.Material.Update;
        protected override string DeletePolicyName { get; set; } = FloorScalePermissions.Material.Delete;

        private readonly IMaterialRepository _repository;
        private readonly IMaterialTypeRepository _maTypeRepository;

        public MaterialAppService(IMaterialRepository repository,
            IMaterialTypeRepository maTypeRepository) : base(repository)
        {
            _repository = repository;
            _maTypeRepository = maTypeRepository;
        }

        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="id">编号</param>
        /// <returns></returns>
        public override async Task<MaterialDto> GetAsync(Guid id)
        {
            var model = await _repository.GetAsync(id);
            return ObjectMapper.Map<Material, MaterialDto>(model);
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public override async Task<PagedResultDto<MaterialDto>> GetListAsync(
            PagedAndSortedResultRequestDto input)
        {
            await CheckGetListPolicyAsync();

            if (input.Sorting.IsNullOrWhiteSpace())
            {
                input.Sorting = nameof(Material.Orders) + " desc";
            }

            var query = from info in Repository
                        join classRe in _maTypeRepository on info.TypeId equals classRe.Id into temp
                        from classJoin in temp.DefaultIfEmpty()
                        select new { info, classJoin };

            query = query
                //.WhereIf(
                //    !input.Filter.IsNullOrWhiteSpace(),
                //    (p => p.info.Name.Contains(input.Filter) || p.info.Code.Contains(input.Filter))
                //)
                //.WhereIf(
                //    input.TypeId != null,
                //    (p => p.info.TypeId == input.TypeId)
                //)
                .OrderBy("info." + input.Sorting)
                .Skip(input.SkipCount)
                .Take(input.MaxResultCount);

            var queryResult = await AsyncExecuter.ToListAsync(query);

            var infoDtos = queryResult.Select(x =>
            {
                var infoDto = ObjectMapper.Map<Material, MaterialDto>(x.info);
                infoDto.TypeName = x.classJoin == null ? null : x.classJoin.Name;
                return infoDto;
            }).ToList();

            var totalCount = await AsyncExecuter.CountAsync(
                _repository
                //.WhereIf(
                //    !input.Filter.IsNullOrWhiteSpace(),
                //    model => model.Name.Contains(input.Filter) || model.Code.Contains(input.Filter)
                //)
                //.WhereIf(
                //    input.TypeId != null,
                //    (model => model.TypeId == input.TypeId)
                //)
            );

            return new PagedResultDto<MaterialDto>(
                totalCount,
                infoDtos);
        }

        /// <summary>
        /// 获取树状列表
        /// </summary>
        /// <returns></returns>
        public async Task<ListResultDto<TreeviewDto>> GetTreeviewAsync(PagedAndSortedResultRequestDto input)
        {
            var query = _repository
                .Where(p => p.Status == true)
                //.WhereIf(
                //    !input.Filter.IsNullOrWhiteSpace(),
                //    model => model.Name.Contains(input.Filter) || model.Code.Contains(input.Filter)
                //)
                //.WhereIf(
                //    input.TypeId != null,
                //    model => model.TypeId == input.TypeId
                //)
                .OrderByDescending(p => p.Orders)
                //.Skip(input.SkipCount)
                //.Take(input.MaxResultCount)
                ;

            var queryResult = await AsyncExecuter.ToListAsync(query);

            return new ListResultDto<TreeviewDto>(
                ObjectMapper.Map<List<Material>, List<TreeviewDto>>(
                    queryResult
                )
            );
        }

        /// <summary>
        /// 创建数据
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [Authorize(FloorScalePermissions.Material.Create)]
        public override async Task<MaterialDto> CreateAsync(CreateMaterialDto input)
        {
            input.TenantId = CurrentTenant.Id;
            var checkInput = await _repository.FindAsync(p => p.Name == input.Name);
            if (checkInput != null)
            {
                throw new AlreadyExistsNameException(input.Name);
            }

            var inputModel = ObjectMapper.Map<CreateMaterialDto, Material>(input);

            var model = await _repository.InsertAsync(inputModel);

            return ObjectMapper.Map<Material, MaterialDto>(model);
        }

        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="id"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        [Authorize(FloorScalePermissions.Material.Update)]
        public override async Task<MaterialDto> UpdateAsync(Guid id, UpdateMaterialDto input)
        {
            var model = await _repository.GetAsync(id);
            if (model.Name != input.Name)
            {
                var checkInput = await _repository.FindAsync(p => p.Name == input.Name);
                if (checkInput != null && checkInput.Id != id)
                {
                    throw new AlreadyExistsNameException(input.Name);
                }
                model.Name = input.Name;
            }
            model.Code = input.Code;
            model.TypeId = input.TypeId;
            model.Price = input.Price;
            model.Status = input.Status;
            model.Remark = input.Remark;
            model.Orders = input.Orders;

            await _repository.UpdateAsync(model);

            return ObjectMapper.Map<Material, MaterialDto>(model);
        }

        /// <summary>
        /// 删除实体
        /// </summary>
        /// <param name="id">编号</param>
        /// <returns></returns>
        [Authorize(FloorScalePermissions.Material.Delete)]
        public override async Task DeleteAsync(Guid id)
        {
            await _repository.DeleteAsync(id);
        }

        /// <summary>
        /// 获取最大排序
        /// </summary>
        /// <returns></returns>
        public async Task<int> GetOrdersAsync()
        {
            await CheckCreatePolicyAsync();

            return (_repository.Max(p => (int?)p.Orders) ?? 0) + 3;
        }
    }
}
