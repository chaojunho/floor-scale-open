using FloorScale.Base.MaterialFormats.Dtos;
using FloorScale.Base.Materials;
using FloorScale.Permissions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace FloorScale.Base.MaterialFormats
{
    public class MaterialFormatAppService : CrudAppService<MaterialFormat, MaterialFormatDto, Guid, PagedAndSortedResultRequestDto, CreateMaterialFormatDto, UpdateMaterialFormatDto>,
        IMaterialFormatAppService
    {
        protected override string GetPolicyName { get; set; } = FloorScalePermissions.MaterialFormat.Default;
        protected override string GetListPolicyName { get; set; } = FloorScalePermissions.MaterialFormat.Default;
        protected override string CreatePolicyName { get; set; } = FloorScalePermissions.MaterialFormat.Create;
        protected override string UpdatePolicyName { get; set; } = FloorScalePermissions.MaterialFormat.Update;
        protected override string DeletePolicyName { get; set; } = FloorScalePermissions.MaterialFormat.Delete;

        private readonly IMaterialFormatRepository _repository;
        private readonly IMaterialRepository _materialRepository;

        public MaterialFormatAppService(
            IMaterialFormatRepository repository,
            IMaterialRepository materialRepository)
            : base(repository)
        {
            _repository = repository;
            _materialRepository = materialRepository;
        }
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="id">编号</param>
        /// <returns></returns>
        public override async Task<MaterialFormatDto> GetAsync(Guid id)
        {
            var model = await _repository.GetAsync(id);
            return ObjectMapper.Map<MaterialFormat, MaterialFormatDto>(model);
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public override async Task<PagedResultDto<MaterialFormatDto>> GetListAsync(
            PagedAndSortedResultRequestDto input)
        {
            await CheckGetListPolicyAsync();

            if (input.Sorting.IsNullOrWhiteSpace())
            {
                input.Sorting = nameof(MaterialFormat.Orders) + " desc";
            }

            var query = from info in Repository
                        join classRe in _materialRepository on info.MaterialId equals classRe.Id into temp
                        from classJoin in temp.DefaultIfEmpty()
                        select new { info, classJoin };

            query = query
                //.WhereIf(
                //    !input.Filter.IsNullOrWhiteSpace(),
                //    (p => p.info.Name.Contains(input.Filter))
                //)
                //.WhereIf(
                //    input.MaterialId != null,
                //    (p => p.info.MaterialId == input.MaterialId)
                //)
                .OrderBy("info." + input.Sorting)
                .Skip(input.SkipCount)
                .Take(input.MaxResultCount);

            var queryResult = await AsyncExecuter.ToListAsync(query);

            var infoDtos = queryResult.Select(x =>
            {
                var infoDto = ObjectMapper.Map<MaterialFormat, MaterialFormatDto>(x.info);
                infoDto.MaterialName = x.classJoin == null ? null : x.classJoin.Name;
                return infoDto;
            }).ToList();

            var totalCount = await AsyncExecuter.CountAsync(
                _repository
                //.WhereIf(
                //    !input.Filter.IsNullOrWhiteSpace(),
                //    model => model.Name.Contains(input.Filter)
                //)
                //.WhereIf(
                //    input.MaterialId != null,
                //    (model => model.MaterialId == input.MaterialId)
                //)
            );

            return new PagedResultDto<MaterialFormatDto>(
                totalCount,
                infoDtos);
        }

        /// <summary>
        /// 获取树状列表
        /// </summary>
        /// <returns></returns>
        public async Task<ListResultDto<TreeviewDto>> GetTreeviewAsync(PagedAndSortedResultRequestDto input)
        {
            var query = _repository
                .Where(p => p.Status == true)
                //.WhereIf(
                //    !input.Filter.IsNullOrWhiteSpace(),
                //    model => model.Name.Contains(input.Filter)
                //)
                //.WhereIf(
                //    input.MaterialId != null,
                //    model => model.MaterialId == input.MaterialId
                //)
                .OrderByDescending(p => p.Orders);

            var queryResult = await AsyncExecuter.ToListAsync(query);

            return new ListResultDto<TreeviewDto>(
                ObjectMapper.Map<List<MaterialFormat>, List<TreeviewDto>>(
                    queryResult
                )
            );
        }

        /// <summary>
        /// 创建数据
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [Authorize(FloorScalePermissions.Material.Create)]
        public override async Task<MaterialFormatDto> CreateAsync(CreateMaterialFormatDto input)
        {
            input.TenantId = CurrentTenant.Id;
            var checkInput = await _repository.FindAsync(p => p.Name == input.Name && p.MaterialId == input.MaterialId);
            if (checkInput != null)
            {
                throw new AlreadyExistsNameException(input.Name);
            }

            var inputModel = ObjectMapper.Map<CreateMaterialFormatDto, MaterialFormat>(input);

            var model = await _repository.InsertAsync(inputModel);

            return ObjectMapper.Map<MaterialFormat, MaterialFormatDto>(model);
        }

        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="id"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        [Authorize(FloorScalePermissions.Material.Update)]
        public override async Task<MaterialFormatDto> UpdateAsync(Guid id, UpdateMaterialFormatDto input)
        {
            var model = await _repository.GetAsync(id);
            model.MaterialId = input.MaterialId;
            if (model.Name != input.Name)
            {
                var checkInput = await _repository.FindAsync(p =>
                    p.Name == input.Name &&
                    p.MaterialId == input.MaterialId);
                if (checkInput != null && checkInput.Id != id)
                {
                    throw new AlreadyExistsNameException(input.Name);
                }
                model.Name = input.Name;
            }
            model.Price = input.Price;
            model.Status = input.Status;
            model.Remark = input.Remark;
            model.Orders = input.Orders;

            await _repository.UpdateAsync(model);

            return ObjectMapper.Map<MaterialFormat, MaterialFormatDto>(model);
        }

        /// <summary>
        /// 删除实体
        /// </summary>
        /// <param name="id">编号</param>
        /// <returns></returns>
        [Authorize(FloorScalePermissions.Material.Delete)]
        public override async Task DeleteAsync(Guid id)
        {
            await _repository.DeleteAsync(id);
        }

        /// <summary>
        /// 获取最大排序
        /// </summary>
        /// <returns></returns>
        public async Task<int> GetOrdersAsync()
        {
            await CheckCreatePolicyAsync();

            return (_repository.Max(p => (int?)p.Orders) ?? 0) + 3;
        }
    }
}
