using System;
using FloorScale.Permissions;
using FloorScale.Base.Cars.Dtos;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace FloorScale.Base.Cars
{
    public class CarAppService : CrudAppService<Car, CarDto, Guid, PagedAndSortedResultRequestDto, CreateCarDto, UpdateCarDto>,
        ICarAppService
    {
        protected override string GetPolicyName { get; set; } = FloorScalePermissions.Car.Default;
        protected override string GetListPolicyName { get; set; } = FloorScalePermissions.Car.Default;
        protected override string CreatePolicyName { get; set; } = FloorScalePermissions.Car.Create;
        protected override string UpdatePolicyName { get; set; } = FloorScalePermissions.Car.Update;
        protected override string DeletePolicyName { get; set; } = FloorScalePermissions.Car.Delete;

        private readonly ICarRepository _repository;
        
        public CarAppService(ICarRepository repository) : base(repository)
        {
            _repository = repository;
        }
    }
}
