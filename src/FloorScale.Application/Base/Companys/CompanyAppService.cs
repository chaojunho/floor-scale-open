using FloorScale.Base.Companys.Dtos;
using FloorScale.Permissions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace FloorScale.Base.Companys
{
    public class CompanyAppService : FloorScaleAppService,
        ICompanyAppService
    {
        private readonly ICompanyRepository _repository;
        
        public CompanyAppService(ICompanyRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// 获取实体
        /// </summary>
        /// <returns></returns>
        public async Task<CompanyDto> GetAsync()
        {
            var model = await _repository.FindAsync(p => p.TenantId == CurrentTenant.Id);
            if (model == null)
            {
                var input = new CreateCompanyDto();
                input.Name = "公司";
                input.TenantId = CurrentTenant.Id;
                var inputModel = ObjectMapper.Map<CreateCompanyDto, Company>(input);
                model = await _repository.InsertAsync(inputModel);
            }

            return ObjectMapper.Map<Company, CompanyDto>(model);
        }

        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [Authorize(FloorScalePermissions.Company.Update)]
        public async Task<CompanyDto> UpdateAsync(UpdateCompanyDto input)
        {
            var model = await _repository.FindAsync(p => p.TenantId == CurrentTenant.Id);
            if (model == null)
            {
                return null;
            }

            model.Name = input.Name;
            model.Code = input.Code;
            model.Contact = input.Contact;
            model.Telephone = input.Telephone;
            model.Mail = input.Mail;
            model.Address = input.Address;
            model.Describe = input.Describe;

            await _repository.UpdateAsync(model);

            return ObjectMapper.Map<Company, CompanyDto>(model);
        }
    }
}
