using System;
using FloorScale.Permissions;
using FloorScale.Base.CarTypes.Dtos;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Collections.Generic;

namespace FloorScale.Base.CarTypes
{
    public class CarTypeAppService : 
        CrudAppService<CarType, CarTypeDto, Guid, GetCarTypeListDto, CreateCarTypeDto, UpdateCarTypeDto>,
        ICarTypeAppService
    {
        protected override string GetPolicyName { get; set; } = FloorScalePermissions.CarType.Default;
        protected override string GetListPolicyName { get; set; } = FloorScalePermissions.CarType.Default;
        protected override string CreatePolicyName { get; set; } = FloorScalePermissions.CarType.Create;
        protected override string UpdatePolicyName { get; set; } = FloorScalePermissions.CarType.Update;
        protected override string DeletePolicyName { get; set; } = FloorScalePermissions.CarType.Delete;

        private readonly ICarTypeRepository _repository;
        private readonly CarTypeManager _manager;

        public CarTypeAppService(
            ICarTypeRepository repository,
            CarTypeManager manager
            ) : base(repository)
        {
            _repository = repository;
            _manager = manager;
        }

        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="id">编号</param>
        /// <returns></returns>
        public override async Task<CarTypeDto> GetAsync(Guid id)
        {
            var model = await _repository.GetAsync(id);
            return ObjectMapper.Map<CarType, CarTypeDto>(model);
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public override async Task<PagedResultDto<CarTypeDto>> GetListAsync(
            GetCarTypeListDto input)
        {
            await CheckGetListPolicyAsync();

            if (input.Sorting.IsNullOrWhiteSpace())
            {
                input.Sorting = nameof(CarType.Orders) + " desc";
            }

            // 根据输入条件查询
            var query = _repository
                .WhereIf(
                    !input.Filter.IsNullOrWhiteSpace(),
                    (p => p.Name.Contains(input.Filter))
                );

            // 获取翻页数据
            var queryTake = query
                .OrderBy(input.Sorting)
                .Skip(input.SkipCount)
                .Take(input.MaxResultCount);

            // 执行数据库查询
            var queryResult = await AsyncExecuter.ToListAsync(queryTake);

            // 内外联查询时使用到，此查询没有内外联，可以忽略
            var infoDtos = queryResult.Select(x =>
            {
                var infoDto = ObjectMapper.Map<CarType, CarTypeDto>(x);
                return infoDto;
            }).ToList();

            // 获取数据条数
            var totalCount = await AsyncExecuter.CountAsync(query);

            return new PagedResultDto<CarTypeDto>(
                totalCount,
                infoDtos);
        }

        /// <summary>
        /// 获取树状列表
        /// </summary>
        /// <returns></returns>
        public async Task<ListResultDto<TreeviewDto>> GetTreeviewAsync(string input)
        {
            var query = _repository
                .Where(p => p.Status == true)
                .WhereIf(
                    !input.IsNullOrWhiteSpace(),
                    model => model.Name.Contains(input) || model.Code.Contains(input)
                )
                .OrderByDescending(p => p.Orders);

            var queryResult = await AsyncExecuter.ToListAsync(query);

            return new ListResultDto<TreeviewDto>(
                ObjectMapper.Map<List<CarType>, List<TreeviewDto>>(
                    queryResult
                )
            );
        }

        /// <summary>
        /// 创建数据
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [Authorize(FloorScalePermissions.Car.Create)]
        public override async Task<CarTypeDto> CreateAsync(CreateCarTypeDto input)
        {
            // 获取当前租户
            input.TenantId = CurrentTenant.Id;
            // 数据映射
            var inputModel = ObjectMapper.Map<CreateCarTypeDto, CarType>(input);
            // 检查实体合法性
            await _manager.CreateAsync(inputModel);

            // 创建数据
            var model = await _repository.InsertAsync(inputModel);
            // 不用再进行save处理，abp会自动处理

            return ObjectMapper.Map<CarType, CarTypeDto>(model);
        }

        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="id"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        [Authorize(FloorScalePermissions.Car.Update)]
        public override async Task<CarTypeDto> UpdateAsync(Guid id, UpdateCarTypeDto input)
        {
            var model = await _repository.GetAsync(id);
            if (model.Name != input.Name)
            {
                await _manager.ChangeNameAsync(model, input.Name);
            }
            model.Code = input.Code;
            model.Status = input.Status;
            model.Remark = input.Remark;
            model.Orders = input.Orders;

            model = await _repository.UpdateAsync(model, autoSave: true);

            return ObjectMapper.Map<CarType, CarTypeDto>(model);
        }

        /// <summary>
        /// 删除实体
        /// </summary>
        /// <param name="id">编号</param>
        /// <returns></returns>
        [Authorize(FloorScalePermissions.Car.Delete)]
        public override async Task DeleteAsync(Guid id)
        {
            // 这里只是软删除
            await _repository.DeleteAsync(id);
        }

        /// <summary>
        /// 获取最大排序
        /// </summary>
        /// <returns></returns>
        public async Task<int> GetOrdersAsync()
        {
            await CheckCreatePolicyAsync();

            return (_repository.Max(p => (int?)p.Orders) ?? 0) + 3;
        }
    }
}
