using FloorScale.Base.Logs.Dtos;
using FloorScale.Permissions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace FloorScale.Base.Logs
{
    public class LogAppService : CrudAppService<Log, LogDto, Guid, GetLogListDto, CreateLogDto, UpdateLogDto>,
        ILogAppService
    {
        protected override string GetPolicyName { get; set; } = FloorScalePermissions.Log.Default;
        protected override string GetListPolicyName { get; set; } = FloorScalePermissions.Log.Default;
        protected override string CreatePolicyName { get; set; } = FloorScalePermissions.Log.Create;
        protected override string UpdatePolicyName { get; set; } = FloorScalePermissions.Log.Update;
        protected override string DeletePolicyName { get; set; } = FloorScalePermissions.Log.Delete;

        private readonly ILogRepository _repository;
        
        public LogAppService(ILogRepository repository) : base(repository)
        {
            _repository = repository;
        }
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="id">编号</param>
        /// <returns></returns>
        public override async Task<LogDto> GetAsync(Guid id)
        {
            var model = await _repository.GetAsync(id);
            return ObjectMapper.Map<Log, LogDto>(model);
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public override async Task<PagedResultDto<LogDto>> GetListAsync(
            GetLogListDto input)
        {
            await CheckGetListPolicyAsync();

            var query = _repository
                .WhereIf(
                    !input.Filter.IsNullOrWhiteSpace(),
                    (p => p.Action.Contains(input.Filter))
                )
                .WhereIf(
                    !input.FrmClientNO.IsNullOrWhiteSpace(),
                    (p => p.FrmClientNO == input.FrmClientNO)
                )
                .OrderByDescending(p => p.CreationTime)
                .Skip(input.SkipCount)
                .Take(input.MaxResultCount);

            var queryResult = await AsyncExecuter.ToListAsync(query);

            var infoDtos = queryResult.Select(x =>
            {
                var infoDto = ObjectMapper.Map<Log, LogDto>(x);
                return infoDto;
            }).ToList();

            var totalCount = await AsyncExecuter.CountAsync(
                _repository.WhereIf(
                    !input.Filter.IsNullOrWhiteSpace(),
                    p => p.Action.Contains(input.Filter)
                )
                .WhereIf(
                    !input.FrmClientNO.IsNullOrWhiteSpace(),
                    (p => p.FrmClientNO == input.FrmClientNO)
                )
            );

            return new PagedResultDto<LogDto>(
                totalCount,
                infoDtos);
        }

        /// <summary>
        /// 创建数据
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [Authorize(FloorScalePermissions.Log.Create)]
        public override async Task<LogDto> CreateAsync(CreateLogDto input)
        {
            input.TenantId = CurrentTenant.Id;
            var inputModel = ObjectMapper.Map<CreateLogDto, Log>(input);

            var model = await _repository.InsertAsync(inputModel);

            return ObjectMapper.Map<Log, LogDto>(model);
        }

        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="id"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        [Authorize(FloorScalePermissions.Log.Update)]
        public override async Task<LogDto> UpdateAsync(Guid id, UpdateLogDto input)
        {
            var model = await _repository.GetAsync(id);
            model.Action = input.Action;
            model.Remark = input.Remark;
            model.FrmClientNO = input.FrmClientNO;

            await _repository.UpdateAsync(model);

            return ObjectMapper.Map<Log, LogDto>(model);
        }

        /// <summary>
        /// 删除实体
        /// </summary>
        /// <param name="id">编号</param>
        /// <returns></returns>
        [Authorize(FloorScalePermissions.Log.Delete)]
        public override async Task DeleteAsync(Guid id)
        {
            await _repository.DeleteAsync(id);
        }
    }
}
