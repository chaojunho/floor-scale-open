using FloorScale.Base.CarTypes;
using FloorScale.Base.CarTypes.Dtos;
using FloorScale.Base.Cars;
using FloorScale.Base.Cars.Dtos;
using FloorScale.Base.Companys;
using FloorScale.Base.Companys.Dtos;
using FloorScale.Base.Logs;
using FloorScale.Base.Logs.Dtos;
using FloorScale.Base.MaterialTypes;
using FloorScale.Base.MaterialTypes.Dtos;
using FloorScale.Base.Materials;
using FloorScale.Base.Materials.Dtos;
using FloorScale.Base.MaterialFormats;
using FloorScale.Base.MaterialFormats.Dtos;
using AutoMapper;
using FloorScale.Base;

namespace FloorScale
{
    public class FloorScaleApplicationAutoMapperProfile : Profile
    {
        public FloorScaleApplicationAutoMapperProfile()
        {
            /* You can configure your AutoMapper mapping configuration here.
             * Alternatively, you can split your mapping configurations
             * into multiple profile classes for a better organization. */
            CreateMap<CarType, CarTypeDto>();
            CreateMap<CarType, TreeviewDto>();
            CreateMap<CreateCarTypeDto, CarType>(MemberList.Source);
            CreateMap<UpdateCarTypeDto, CarType>(MemberList.Source);

            CreateMap<Car, CarDto>();
            CreateMap<Car, TreeviewDto>();
            CreateMap<CreateCarDto, Car>(MemberList.Source);
            CreateMap<UpdateCarDto, Car>(MemberList.Source);

            CreateMap<Company, CompanyDto>();
            CreateMap<CreateCompanyDto, Company>(MemberList.Source);
            CreateMap<UpdateCompanyDto, Company>(MemberList.Source);

            CreateMap<Log, LogDto>();
            CreateMap<CreateLogDto, Log>(MemberList.Source);
            CreateMap<UpdateLogDto, Log>(MemberList.Source);

            CreateMap<MaterialType, MaterialTypeDto>();
            CreateMap<MaterialType, TreeviewDto>();
            CreateMap<CreateMaterialTypeDto, MaterialType>(MemberList.Source);
            CreateMap<UpdateMaterialTypeDto, MaterialType>(MemberList.Source);

            CreateMap<Material, MaterialDto>();
            CreateMap<Material, TreeviewDto>();
            CreateMap<CreateMaterialDto, Material>(MemberList.Source);
            CreateMap<UpdateMaterialDto, Material>(MemberList.Source);

            CreateMap<MaterialFormat, MaterialFormatDto>();
            CreateMap<MaterialFormat, TreeviewDto>();
            CreateMap<CreateMaterialFormatDto, MaterialFormat>(MemberList.Source);
            CreateMap<UpdateMaterialFormatDto, MaterialFormat>(MemberList.Source);
        }
    }
}
