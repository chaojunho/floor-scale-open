﻿using FloorScale.EntityFrameworkCore;
using Volo.Abp.Autofac;
using Volo.Abp.BackgroundJobs;
using Volo.Abp.Modularity;

namespace FloorScale.DbMigrator
{
    [DependsOn(
        typeof(AbpAutofacModule),
        typeof(FloorScaleEntityFrameworkCoreModule),
        typeof(FloorScaleApplicationContractsModule)
        )]
    public class FloorScaleDbMigratorModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpBackgroundJobOptions>(options => options.IsJobExecutionEnabled = false);
        }
    }
}
