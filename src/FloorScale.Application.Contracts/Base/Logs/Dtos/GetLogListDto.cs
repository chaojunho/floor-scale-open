﻿using Volo.Abp.Application.Dtos;

namespace FloorScale.Base.Logs.Dtos
{
    public class GetLogListDto :
        PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
        /// <summary>
        /// 客户端编号
        /// </summary>
        public string FrmClientNO { get; set; }
    }
}
