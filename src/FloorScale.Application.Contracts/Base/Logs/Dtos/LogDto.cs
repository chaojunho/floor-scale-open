using System;
using Volo.Abp.Application.Dtos;

namespace FloorScale.Base.Logs.Dtos
{
    [Serializable]
    public class LogDto : CreationAuditedEntityDto<Guid>
    {
        public string Action { get; set; }

        public string Remark { get; set; }

        public string FrmClientNO { get; set; }
    }
}