using System;
using System.ComponentModel;
namespace FloorScale.Base.Logs.Dtos
{
    [Serializable]
    public class CreateLogDto
    {
        [DisplayName("LogAction")]
        public string Action { get; set; }

        [DisplayName("LogRemark")]
        public string Remark { get; set; }

        [DisplayName("LogFrmClientNO")]
        public string FrmClientNO { get; set; }

        public Guid? TenantId { get; set; }
    }
}