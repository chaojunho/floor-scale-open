using System;
using FloorScale.Base.Logs.Dtos;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace FloorScale.Base.Logs
{
    public interface ILogAppService :
        ICrudAppService< 
            LogDto, 
            Guid,
            GetLogListDto,
            CreateLogDto,
            UpdateLogDto>
    {

    }
}