using System;
using Volo.Abp.Application.Dtos;

namespace FloorScale.Base.MaterialTypes.Dtos
{
    [Serializable]
    public class MaterialTypeDto : FullAuditedEntityDto<Guid>
    {
        public string Name { get; set; }

        public string Code { get; set; }

        public bool Status { get; set; }

        public string Remark { get; set; }

        public int Orders { get; set; }
    }
}