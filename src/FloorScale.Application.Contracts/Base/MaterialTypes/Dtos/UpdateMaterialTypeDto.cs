using System;
using System.ComponentModel;

namespace FloorScale.Base.MaterialTypes.Dtos
{
    [Serializable]
    public class UpdateMaterialTypeDto
    {
        [DisplayName("MaterialTypeName")]
        public string Name { get; set; }

        [DisplayName("MaterialTypeCode")]
        public string Code { get; set; }

        [DisplayName("MaterialTypeStatus")]
        public bool Status { get; set; }

        [DisplayName("MaterialTypeRemark")]
        public string Remark { get; set; }

        [DisplayName("MaterialTypeOrders")]
        public int Orders { get; set; }
    }
}