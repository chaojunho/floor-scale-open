using System;
using FloorScale.Base.MaterialTypes.Dtos;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace FloorScale.Base.MaterialTypes
{
    public interface IMaterialTypeAppService :
        ICrudAppService< 
            MaterialTypeDto, 
            Guid, 
            PagedAndSortedResultRequestDto,
            CreateMaterialTypeDto,
            UpdateMaterialTypeDto>
    {

    }
}