using System;
using System.ComponentModel;

namespace FloorScale.Base.Cars.Dtos
{
    [Serializable]
    public class UpdateCarDto
    {
        [DisplayName("CarLicensePlate")]
        public string LicensePlate { get; set; }

        [DisplayName("CarCarOwnerName")]
        public string CarOwnerName { get; set; }

        [DisplayName("CarCarOwnerIDCard")]
        public string CarOwnerIDCard { get; set; }

        [DisplayName("CarCarOwnerPhone")]
        public string CarOwnerPhone { get; set; }

        [DisplayName("CarTypeId")]
        public Guid? TypeId { get; set; }

        [DisplayName("CarShipperId")]
        public Guid? ShipperId { get; set; }

        [DisplayName("CarReceivingUnitId")]
        public Guid? ReceivingUnitId { get; set; }

        [DisplayName("CarMaterialId")]
        public Guid? MaterialId { get; set; }

        [DisplayName("CarMaterialFormatId")]
        public Guid? MaterialFormatId { get; set; }

        [DisplayName("CarWeightTypeId")]
        public Guid? WeightTypeId { get; set; }

        [DisplayName("CarStatus")]
        public bool Status { get; set; }

        [DisplayName("CarRemark")]
        public string Remark { get; set; }

        [DisplayName("CarOrders")]
        public int Orders { get; set; }
    }
}