using System;
using Volo.Abp.Application.Dtos;

namespace FloorScale.Base.Cars.Dtos
{
    [Serializable]
    public class CarDto : FullAuditedEntityDto<Guid>
    {
        public string LicensePlate { get; set; }

        public string CarOwnerName { get; set; }

        public string CarOwnerIDCard { get; set; }

        public string CarOwnerPhone { get; set; }

        public Guid? TypeId { get; set; }

        public Guid? ShipperId { get; set; }

        public Guid? ReceivingUnitId { get; set; }

        public Guid? MaterialId { get; set; }

        public Guid? MaterialFormatId { get; set; }

        public Guid? WeightTypeId { get; set; }

        public bool Status { get; set; }

        public string Remark { get; set; }

        public int Orders { get; set; }
    }
}