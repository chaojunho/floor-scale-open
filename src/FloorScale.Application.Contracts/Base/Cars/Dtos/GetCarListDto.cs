﻿using System;
using Volo.Abp.Application.Dtos;

namespace FloorScale.Base.Cars.Dtos
{
    public class GetCarListDto :
           PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
        /// <summary>
        /// 类型编号
        /// </summary>
        public Guid? TypeId { get; set; }
        /// <summary>
        /// 送货单位编号
        /// </summary>
        public Guid? ShipperId { get; set; }
        /// <summary>
        /// 物料编号
        /// </summary>
        public Guid? MaterialId { get; set; }
        /// <summary>
        /// 过磅类型编号
        /// </summary>
        public Guid? WeightTypeId { get; set; }
    }
}
