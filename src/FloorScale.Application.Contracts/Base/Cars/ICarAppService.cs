using System;
using FloorScale.Base.Cars.Dtos;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace FloorScale.Base.Cars
{
    public interface ICarAppService :
        ICrudAppService< 
            CarDto, 
            Guid, 
            PagedAndSortedResultRequestDto,
            CreateCarDto,
            UpdateCarDto>
    {

    }
}