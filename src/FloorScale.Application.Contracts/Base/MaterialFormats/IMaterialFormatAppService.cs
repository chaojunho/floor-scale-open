using System;
using FloorScale.Base.MaterialFormats.Dtos;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace FloorScale.Base.MaterialFormats
{
    public interface IMaterialFormatAppService :
        ICrudAppService< 
            MaterialFormatDto, 
            Guid, 
            PagedAndSortedResultRequestDto,
            CreateMaterialFormatDto,
            UpdateMaterialFormatDto>
    {

    }
}