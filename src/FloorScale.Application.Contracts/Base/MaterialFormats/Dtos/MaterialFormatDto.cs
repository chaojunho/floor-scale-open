using System;
using Volo.Abp.Application.Dtos;

namespace FloorScale.Base.MaterialFormats.Dtos
{
    [Serializable]
    public class MaterialFormatDto : FullAuditedEntityDto<Guid>
    {
        public Guid MaterialId { get; set; }
        public string MaterialName { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }

        public bool Status { get; set; }

        public string Remark { get; set; }

        public int Orders { get; set; }
    }
}