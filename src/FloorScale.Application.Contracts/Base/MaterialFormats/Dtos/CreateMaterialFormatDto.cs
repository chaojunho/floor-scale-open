using System;
using System.ComponentModel;
namespace FloorScale.Base.MaterialFormats.Dtos
{
    [Serializable]
    public class CreateMaterialFormatDto
    {
        [DisplayName("MaterialFormatMaterialId")]
        public Guid MaterialId { get; set; }

        [DisplayName("MaterialFormatName")]
        public string Name { get; set; }

        [DisplayName("MaterialFormatPrice")]
        public decimal Price { get; set; }

        [DisplayName("MaterialFormatStatus")]
        public bool Status { get; set; }

        [DisplayName("MaterialFormatRemark")]
        public string Remark { get; set; }

        [DisplayName("MaterialFormatOrders")]
        public int Orders { get; set; }

        public Guid? TenantId { get; set; }
    }
}