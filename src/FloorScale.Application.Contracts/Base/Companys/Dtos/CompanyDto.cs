using System;
using Volo.Abp.Application.Dtos;

namespace FloorScale.Base.Companys.Dtos
{
    [Serializable]
    public class CompanyDto : FullAuditedEntityDto<Guid>
    {
        public string Name { get; set; }

        public string Code { get; set; }

        public string BusinessLicense { get; set; }

        public string Contact { get; set; }

        public string Telephone { get; set; }

        public string Mail { get; set; }

        public string Address { get; set; }

        public string Describe { get; set; }
    }
}