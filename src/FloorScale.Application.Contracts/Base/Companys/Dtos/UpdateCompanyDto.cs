using System;
using System.ComponentModel;

namespace FloorScale.Base.Companys.Dtos
{
    [Serializable]
    public class UpdateCompanyDto
    {
        [DisplayName("CompanyName")]
        public string Name { get; set; }

        [DisplayName("CompanyCode")]
        public string Code { get; set; }

        [DisplayName("CompanyBusinessLicense")]
        public string BusinessLicense { get; set; }

        [DisplayName("CompanyContact")]
        public string Contact { get; set; }

        [DisplayName("CompanyTelephone")]
        public string Telephone { get; set; }

        [DisplayName("CompanyMail")]
        public string Mail { get; set; }

        [DisplayName("CompanyAddress")]
        public string Address { get; set; }

        [DisplayName("CompanyDescribe")]
        public string Describe { get; set; }
    }
}