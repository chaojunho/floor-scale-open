using System;
using FloorScale.Base.Materials.Dtos;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace FloorScale.Base.Materials
{
    public interface IMaterialAppService :
        ICrudAppService< 
            MaterialDto, 
            Guid, 
            PagedAndSortedResultRequestDto,
            CreateMaterialDto,
            UpdateMaterialDto>
    {

    }
}