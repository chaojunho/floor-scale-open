using System;
using System.ComponentModel;
namespace FloorScale.Base.Materials.Dtos
{
    [Serializable]
    public class CreateMaterialDto
    {
        [DisplayName("MaterialName")]
        public string Name { get; set; }

        [DisplayName("MaterialCode")]
        public string Code { get; set; }

        [DisplayName("MaterialTypeId")]
        public Guid? TypeId { get; set; }

        [DisplayName("MaterialPrice")]
        public decimal Price { get; set; }

        [DisplayName("MaterialStatus")]
        public bool Status { get; set; }

        [DisplayName("MaterialRemark")]
        public string Remark { get; set; }

        [DisplayName("MaterialOrders")]
        public int Orders { get; set; }

        public Guid? TenantId { get; set; }
    }
}