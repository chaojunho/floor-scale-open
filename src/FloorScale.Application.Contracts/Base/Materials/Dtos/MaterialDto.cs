using System;
using Volo.Abp.Application.Dtos;

namespace FloorScale.Base.Materials.Dtos
{
    [Serializable]
    public class MaterialDto : FullAuditedEntityDto<Guid>
    {
        public string Name { get; set; }

        public string Code { get; set; }

        public Guid? TypeId { get; set; }

        public string TypeName { get; set; }

        public decimal Price { get; set; }

        public bool Status { get; set; }

        public string Remark { get; set; }

        public int Orders { get; set; }
    }
}