using FloorScale.Base.CarTypes.Dtos;
using System;
using Volo.Abp.Application.Services;

namespace FloorScale.Base.CarTypes
{
    public interface ICarTypeAppService :
        ICrudAppService< 
            CarTypeDto, 
            Guid,
            GetCarTypeListDto,
            CreateCarTypeDto,
            UpdateCarTypeDto>
    {

    }
}