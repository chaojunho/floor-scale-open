﻿using Volo.Abp.Application.Dtos;

namespace FloorScale.Base.CarTypes.Dtos
{
    public class GetCarTypeListDto :
        PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
