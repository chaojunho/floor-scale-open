using System;
using System.ComponentModel;
namespace FloorScale.Base.CarTypes.Dtos
{
    [Serializable]
    public class CreateCarTypeDto
    {
        [DisplayName("CarTypeName")]
        public string Name { get; set; }

        [DisplayName("CarTypeCode")]
        public string Code { get; set; }

        [DisplayName("CarTypeStatus")]
        public bool Status { get; set; }

        [DisplayName("CarTypeRemark")]
        public string Remark { get; set; }

        [DisplayName("CarTypeOrders")]
        public int Orders { get; set; }

        public Guid? TenantId { get; set; }

    }
}