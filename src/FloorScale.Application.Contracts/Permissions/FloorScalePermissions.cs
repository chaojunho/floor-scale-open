namespace FloorScale.Permissions
{
    public static class FloorScalePermissions
    {
        public const string GroupName = "FloorScale";

        //Add your own permission names. Example:
        //public const string MyPermission1 = GroupName + ".MyPermission1";

        public class CarType
        {
            public const string Default = GroupName + ".CarType";
            public const string Update = Default + ".Update";
            public const string Create = Default + ".Create";
            public const string Delete = Default + ".Delete";
        }

        public class Car
        {
            public const string Default = GroupName + ".Car";
            public const string Update = Default + ".Update";
            public const string Create = Default + ".Create";
            public const string Delete = Default + ".Delete";
        }

        public class Company
        {
            public const string Default = GroupName + ".Company";
            public const string Update = Default + ".Update";
            public const string Create = Default + ".Create";
            public const string Delete = Default + ".Delete";
        }

        public class Log
        {
            public const string Default = GroupName + ".Log";
            public const string Update = Default + ".Update";
            public const string Create = Default + ".Create";
            public const string Delete = Default + ".Delete";
        }

        public class MaterialType
        {
            public const string Default = GroupName + ".MaterialType";
            public const string Update = Default + ".Update";
            public const string Create = Default + ".Create";
            public const string Delete = Default + ".Delete";
        }

        public class Material
        {
            public const string Default = GroupName + ".Material";
            public const string Update = Default + ".Update";
            public const string Create = Default + ".Create";
            public const string Delete = Default + ".Delete";
        }

        public class MaterialFormat
        {
            public const string Default = GroupName + ".MaterialFormat";
            public const string Update = Default + ".Update";
            public const string Create = Default + ".Create";
            public const string Delete = Default + ".Delete";
        }
    }
}
