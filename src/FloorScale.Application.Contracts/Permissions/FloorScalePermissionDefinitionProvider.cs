using FloorScale.Localization;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;

namespace FloorScale.Permissions
{
    public class FloorScalePermissionDefinitionProvider : PermissionDefinitionProvider
    {
        public override void Define(IPermissionDefinitionContext context)
        {
            var myGroup = context.AddGroup(FloorScalePermissions.GroupName);
            //Define your own permissions here. Example:
            //myGroup.AddPermission(FloorScalePermissions.MyPermission1, L("Permission:MyPermission1"));

            var carTypePermission = myGroup.AddPermission(FloorScalePermissions.CarType.Default, L("Permission:CarType"));
            carTypePermission.AddChild(FloorScalePermissions.CarType.Create, L("Permission:Create"));
            carTypePermission.AddChild(FloorScalePermissions.CarType.Update, L("Permission:Update"));
            carTypePermission.AddChild(FloorScalePermissions.CarType.Delete, L("Permission:Delete"));

            var carPermission = myGroup.AddPermission(FloorScalePermissions.Car.Default, L("Permission:Car"));
            carPermission.AddChild(FloorScalePermissions.Car.Create, L("Permission:Create"));
            carPermission.AddChild(FloorScalePermissions.Car.Update, L("Permission:Update"));
            carPermission.AddChild(FloorScalePermissions.Car.Delete, L("Permission:Delete"));

            var companyPermission = myGroup.AddPermission(FloorScalePermissions.Company.Default, L("Permission:Company"));
            companyPermission.AddChild(FloorScalePermissions.Company.Create, L("Permission:Create"));
            companyPermission.AddChild(FloorScalePermissions.Company.Update, L("Permission:Update"));
            companyPermission.AddChild(FloorScalePermissions.Company.Delete, L("Permission:Delete"));

            var logPermission = myGroup.AddPermission(FloorScalePermissions.Log.Default, L("Permission:Log"));
            logPermission.AddChild(FloorScalePermissions.Log.Create, L("Permission:Create"));
            logPermission.AddChild(FloorScalePermissions.Log.Update, L("Permission:Update"));
            logPermission.AddChild(FloorScalePermissions.Log.Delete, L("Permission:Delete"));

            var materialTypePermission = myGroup.AddPermission(FloorScalePermissions.MaterialType.Default, L("Permission:MaterialType"));
            materialTypePermission.AddChild(FloorScalePermissions.MaterialType.Create, L("Permission:Create"));
            materialTypePermission.AddChild(FloorScalePermissions.MaterialType.Update, L("Permission:Update"));
            materialTypePermission.AddChild(FloorScalePermissions.MaterialType.Delete, L("Permission:Delete"));

            var materialPermission = myGroup.AddPermission(FloorScalePermissions.Material.Default, L("Permission:Material"));
            materialPermission.AddChild(FloorScalePermissions.Material.Create, L("Permission:Create"));
            materialPermission.AddChild(FloorScalePermissions.Material.Update, L("Permission:Update"));
            materialPermission.AddChild(FloorScalePermissions.Material.Delete, L("Permission:Delete"));

            var materialFormatPermission = myGroup.AddPermission(FloorScalePermissions.MaterialFormat.Default, L("Permission:MaterialFormat"));
            materialFormatPermission.AddChild(FloorScalePermissions.MaterialFormat.Create, L("Permission:Create"));
            materialFormatPermission.AddChild(FloorScalePermissions.MaterialFormat.Update, L("Permission:Update"));
            materialFormatPermission.AddChild(FloorScalePermissions.MaterialFormat.Delete, L("Permission:Delete"));
        }

        private static LocalizableString L(string name)
        {
            return LocalizableString.Create<FloorScaleResource>(name);
        }
    }
}
